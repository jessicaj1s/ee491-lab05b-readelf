///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04a - Memory Scanner
///
/// @file readelf.c
/// @version 1.0
///
/// @author Jessica Jones <jjones2@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   22_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void info(); 

int main(int argc, char * argv[]) {
   /// use to check if flag is supported
   int c;

   /// use while loop to check for multiple flags
   /// use switch case in event more flags are used
	while ((c = getopt (argc, argv, "h:")) != -1){
    	switch (c){
      	case 'h':
            printf("header\r\n"); 
	      break;
         default: 
            info(); 
	   }
   }
}

void info() {
   fprintf( stdout, "Usage: readelf.c -Flag Filename\r\n"); 
}


